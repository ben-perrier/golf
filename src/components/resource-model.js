const { ObjectId } = require('mongodb')
const _ = require('lodash')

module.exports = {
  name: 'resourceModel',
  deps: ['mongo'],

  async create ({ collection, obj, formatter }) {
    if (!collection) throw Error('resourceModel error: no collection specified')
    if (!obj) throw Error('resourceModel error: no obj specified')

    obj = {
      ...obj,
      createdAt: new Date(),
      deleted: false
    }

    const { ops: [newObject] } = await this.mongo.db.collection(collection)
      .insertOne(obj)

    return this.format(newObject, formatter)
  },

  async get ({ collection, query: { id, ids, ...params } = {}, limit = 0, formatter }) {
    if (!collection) throw Error('resourceModel error: no collection specified')

    const idsfilter = id || ids ? { $in: (ids || [id]).map(id => new ObjectId(id)) } : undefined

    const mongoQuery = _.omitBy({
      _id: idsfilter,
      ...params,
      deleted: false
    }, _.isUndefined)

    const res = await this.mongo.db.collection(collection)
      .find(mongoQuery)
      .limit(limit)
      .toArray()

    return res.map(itm => this.format(itm, formatter))
  },

  async update ({ collection, id, update, formatter }) {
    if (!collection) throw Error('resourceModel error: no collection specified')
    if (!update) throw new Error('Update object must be provided')
    if (!id) throw new Error('Can\'t perform an update without id or ids filter')

    update = {
      ...update,
      updatedAt: new Date()
    }

    const query = {
      _id: new ObjectId(id),
      deleted: false
    }

    const { value } = await this.mongo.db.collection(collection)
      .findOneAndUpdate(_.omitBy(query, _.isUndefined), { $set: _.omitBy(update, _.isUndefined) }, { returnOriginal: false })

    return this.format(value, formatter)
  },

  format (obj, formatter = (itm) => itm) {
    obj = formatter(obj)
    return this.defaultFormat(obj)
  },

  defaultFormat: (obj) => {
    return Object.entries(obj).reduce((acc, [key, value]) => {
      if (key === '_id') key = 'id'
      if (value instanceof ObjectId) value = value.toString()
      return { ...acc, [key]: value }
    }, {})
  }
}
