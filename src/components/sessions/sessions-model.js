const bcrypt = require('bcryptjs')
const { UserInputError } = require('apollo-server')
const jwt = require('jsonwebtoken')

module.exports = {
  name: 'sessionsModel',
  deps: ['usersModel'],

  /**
   * Authenticates user and sets new session token response cookie
   * @param {object} options.email: user email
   * @param {object} options.password: user password
   * @returns {User}: if the user passwords match, returns the User information, otherwise return undefined.
   */
  async create ({ email, password, res }) {
    const [user] = await this.usersModel.get({ email })
    const match = await bcrypt.compareSync(password, user ? user.hash : 'undefined')
    if (!match) throw new UserInputError('Username or password is incorrect')
    this.setCookie(user, res)
    const session = {
      user
    }
    return session
  },

  /**
   * Decodes session information from the token
   * @param {*} options.req: Incoming server request object
   */
  get ({ req }) {
    const debug = false
    try {
      const { iat, ...user } = this.decodeToken(req.cookies[this.config.cookie.name])
      return { user }
    } catch (e) {
      if (debug && e.message === 'jwt must be provided') console.log('Cookie is not set on the request')
      if (debug && e.message === 'jwt malformed') console.log('Token was tampered with')
    }
    return {}
  },

  /**
   * Removes the session token cookie from the response
   * @param {*} options.res: Server response object
   */
  async delete ({ res }) {
    res.clearCookie(this.config.cookie.name, this.config.token.cookiePolicy)
    return true
  },

  setCookie (user, res) {
    const token = this.createToken(user)
    const { name, lifespan, options } = this.config.cookie
    const cookieOptions = {
      expires: new Date(Date.now() + lifespan),
      ...options
    }
    res.cookie(
      name,
      token,
      cookieOptions
    )
  },

  createToken ({ id, email, firstname }) {
    return jwt.sign({ id, email, firstname }, this.config.token.encryptionKey)
  },

  decodeToken (token) {
    return jwt.verify(token, this.config.token.encryptionKey)
  }
}
