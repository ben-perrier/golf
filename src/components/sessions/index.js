module.exports = {
  name: 'sessions',
  deps: ['sessionsModel', 'graphql'],

  init () {
    this.graphql.registerSchema({
      typeDefs: this.graphql.load('sessions.graphql'),
      resolvers: {
        Mutation: {
          createSession: this.createSession,
          deleteSession: this.deleteSession
        },
        Query: {
          session: this.getSession
        }
      }
    })
  },

  async getSession (parent, args, { req }) {
    return this.sessionsModel.get({ req })
  },

  async createSession (parent, args, { res }) {
    return this.sessionsModel.create({ ...args, res })
  },

  async deleteSession (parent, args, { res }) {
    return this.sessionsModel.delete({ res })
  }
}
