const _ = require('lodash')
const { UserInputError } = require('apollo-server')
const ALIASES = ['a', 'b', 'c', 'd']
const { getCard } = require('./card-helpers')

class Board {
  constructor ({ players, deck, events }) {
    this.pickupPile = new PickupPile({ cards: [...deck] })
    this.discardPile = new DiscardPile()
    // deal cards

    this.spots = new Array(4).fill('').reduce((acc, _, spotIndex) => {
      players.forEach(({ alias }) => {
        const card = this.pickupPile.take()
        const id = `${alias}${spotIndex}`
        const spot = new Slot({ card, discardPile: this.discardPile })
        acc[id] = spot
      })
      return acc
    }, players.reduce((acc, { alias }) => ({ ...acc, [`${alias}h`]: new Hand({ discardPile: this.discardPile }) }), {}))
    // flip first card
    const firstCard = this.pickupPile.take()
    this.discardPile.put(firstCard)
    this.game = { players, deck, events }
    this.spots.p = this.pickupPile
    this.spots.d = this.discardPile
  }

  getSpots () {
    return Object.entries(this.spots).reduce((acc, [id, { card, isVisible }]) => {
      if (isVisible) acc.push({ id, card })
      return acc
    }, [])
  }

  playEvents () {
    const { events } = this.game
    for (const { action, ...event } of events) {
      if (!this[action]) throw Error(`Game state error: command ${action} not found`)
      this[action](event)
    }
    return this
  }

  pickup ({ player, pile }) {
    const card = pile === 'DISCARD' ? this.discardPile.take() : this.pickupPile.take()
    this.spots[`${player}h`].put(card)
  }

  place ({ player, spot }) {
    const card = this.spots[`${player}h`].take()
    this.spots[`${player}${spot}`].put(card)
  }

  discard ({ player }) {
    this.spots[`${player}h`].discard()
  }

  reveal ({ player, spot }) {
    this.spots[`${player}${spot}`].reveal()
  }

  getScores () {
    const countOccurences = (array) => array.reduce((acc, itm) => {
      acc[itm] = acc[itm] ? acc[itm]++ : 1
      return acc
    }, {})
    const getScoreValue = (rank) => {
      const heads = {
        'J': 10,
        'Q': 10,
        'K': 0
      }
      return isNaN(rank) ? heads[rank] : parseInt(rank)
    }
    const countScore = (spots) => { // {1: 'J'}
      const groups = countOccurences(spots)
      const [fullSuit] = Object.entries(groups).map(([key, value]) => value === 4)
      if (fullSuit) return -20
      const [triplet] = Object.entries(groups).filter(([key, value]) => value === 3)
      const singlets = Object.entries(groups).filter(([key, value]) => value === 1).map(([key]) => key)
      const total = triplet ? singlets.concat(triplet[0]) : singlets
      return total.reduce((acc, itm) => acc+getScoreValue(itm), 0)
    }
    return this.game.players.reduce((acc, { alias }) => {
      const spots = Object.entries(this.spots)
      .filter(([key]) => new RegExp(`${alias}[0-9]`).test(key))
      .map(([key, { card }]) => getCard(card).rank)
      acc[alias] = countScore(spots)
      return acc
    }, {})
  }
}

class Spot {
  constructor ({ isVisible, card = null }) {
    this.card = card
    this.isVisible = isVisible
  }

  take () {
    const card = this.card
    this.card = null
    return card
  }

  put (newCard) {
    this.card = newCard
  }
}

class Slot extends Spot {
  constructor ({ card, discardPile }) {
    super({ isVisible: false, card })
    this.discardPile = discardPile
  }

  put (newCard) {
    if (this.isVisible) throw UserInputError('This slot is already revealed')
    this.discardPile.put(this.card)
    this.isVisible = true
    return super.put(newCard)
  }

  reveal () {
    if (this.isVisible) throw UserInputError('This slot is already revealed')
    this.isVisible = true
  }

  discard () {
    this.discardPile.put(this.card)
    this.card = null
  }
}

class Hand extends Spot {
  constructor ({ discardPile }) {
    super({ isVisible: true })
    this.discardPile = discardPile
  }

  discard () {
    this.discardPile.put(this.card)
    this.card = null
  }
}

class Pile extends Spot {
  constructor ({ card, isVisible }) {
    super({ card, isVisible })
  }

  take () {
    const card = this.cards.shift()
    this.card = this.cards[0]
    return card
  }
}

class PickupPile extends Pile {
  constructor ({ cards }) {
    super({ card: cards[0], isVisible: false })
    this.cards = cards
  }
}

class DiscardPile extends Pile {
  constructor () {
    super({ isVisible: true })
    this.cards = []
  }

  put (card) {
    this.cards.unshift(card)
    this.card = this.cards[0]
  }
}

const NextTurn = {
  authorise ({ game, event }) {
    const { player, action } = event
    const nextTurn = this.nextTurn(game)
    if (nextTurn.player !== player) throw UserInputError(`It is not player ${player}'s turn to play.`)
    if (!nextTurn.actions.includes(action)) throw UserInputError(`Player ${player} must complete their turn by playing the actions '${nextTurn.actions.join("'  or '")}'`)
    return 'All good!'
  },

  get ({ events, players }) {
    if (!events.length) return { player: 'a', action: 'pickup' }
    const isGameFinished = (wholeTurnsCount) => wholeTurnsCount === players.length * 4

    // Player to complete their turn
    const { player, actions, wholeTurnsCount } = this.getRemainingAction({ events: [...events], numberOfPlayers: players.length })
    if (wholeTurnsCount && isGameFinished(wholeTurnsCount)) return null
    if (player && actions) return { player, actions }

    // NextPlayer
    const lastPlayer = events.slice(-1)[0].player
    const nextPlayerIndex = players.findIndex(({ alias }) => alias === lastPlayer) + 1
    const nextPlayer = players[nextPlayerIndex] ? players[nextPlayerIndex].alias : 'a'
    return { player: nextPlayer, actions: ['pickup'] }
  },

  getRemainingAction ({ events }) {
    const PATTERNS = [
      [{ action: 'pickup', pile: 'DISCARD' }, { action: 'place' }], // side-effect: discard
      [{ action: 'pickup', pile: 'PICKUP' }, { action: 'place' }], // side-effect: discard
      [{ action: 'pickup', pile: 'PICKUP' }, { action: 'discard' }, { action: 'reveal' }]
    ]
    const eventMatches = (event, eventPattern) => _.filter([event], _.matches(eventPattern)).length
    const pathMatches = (eventCollection, pathPattern) => (
      eventCollection.map((ev, index) => eventMatches(ev, pathPattern[index]))
    )
    const isFullMatch = (eventCollection, pathPattern) => pathMatches(eventCollection, pathPattern).every(itm => !!itm)
    const isPartialMatch = (eventCollection, pathPattern) => pathMatches(eventCollection, pathPattern).some(itm => !!itm)

    const getFullMatch = (events) => {
      for (const pattern of PATTERNS) {
        if (events.length < pattern.length) continue
        if (isFullMatch(events, pattern)) return pattern
      }
    }

    const getPartialMatches = (events) => {
      const matchingPatterns = []
      for (const pattern of PATTERNS) {
        if (isPartialMatch(events, pattern)) {
          matchingPatterns.push(pattern)
        }
      }
      return matchingPatterns
    }

    let wholeTurnsCount = 0
    let matchingWholeTurns = true
    // Eliminating fullMatches
    while (matchingWholeTurns) {
      const fullMatch = getFullMatch(events)
      if (fullMatch) events.splice(0, fullMatch.length)
      else matchingWholeTurns = false
      wholeTurnsCount++
    }

    // If nothing is left, next player can play
    if (!events.length) return { wholeTurnsCount }

    // Otherwise get the possible next actions
    const partialMatches = getPartialMatches(events)
    const nextMoves = partialMatches.map(itm => itm[events.length]).filter(itm => !!itm)
    return { player: events[0].player, actions: nextMoves.map(({ action }) => action) }
  }
}

module.exports = { Board, NextTurn, ALIASES }
