const SUITS = ['♥️', '♦️', '♠️', '♣️']
const RANKS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K']

const newDeck = () => new Array(52).fill(0).map((v, i) => i)

const newShuffledDeck = () => newDeck().sort(el => Math.random() - 0.5)

const getCard = (int) => {
  const rank = RANKS[(int % RANKS.length)]
  const suit = SUITS[(int - int % RANKS.length) / 13]
  return {
    id: `${rank}${suit}`,
    suit,
    rank
  }
}


module.exports = { newShuffledDeck, getCard }