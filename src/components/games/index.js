module.exports = {
  name: 'games',
  deps: ['gamesModel', 'graphql'],

  init () {
    this.graphql.registerSchema({
      typeDefs: this.graphql.load('games.graphql'),
      resolvers: {
        Mutation: {
          createGame: this.createGame,
          pickup: this.pickup,
          place: this.place,
          discard: this.discard,
          reveal: this.reveal
        },
        Game: {
          nextTurn: (parent) => this.gamesModel.getNextTurn(parent),
          currentRound: (parent) => this.gamesModel.getCurrentRound(parent)
        }
      }
    })
  },

  async createGame (parent, args, context) {
    return this.gamesModel.addEvent({ ...args })
  },

  async pickup (parent, { game, pile }, { user }) {
    const action = 'pickup'
    return this.gamesModel.addEvent({ id: game, event: { action, pile }, user })
  },

  async place (parent, { game, spot }, { user }) {
    const action = 'place'
    return this.gamesModel.addEvent({ id: game, event: { action, spot }, user })
  },

  async discard (parent, { game }, { user }) {
    const action = 'discard'
    return this.gamesModel.addEvent({ id: game, event: { action }, user })
  },

  async reveal (parent, { game, spot }, { user }) {
    const action = 'reveal'
    return this.gamesModel.addEvent({ id: game, event: { action, spot }, user })
  }

}
