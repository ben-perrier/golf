const { UserInputError } = require('apollo-server')
const { newShuffledDeck } = require('./card-helpers') 
const { Board, NextTurn, ALIASES } = require('./boards-utilities')

module.exports = {
  name: 'gamesModel',
  collection: 'games',
  deps: ['resourceModel', 'usersModel'],

  async create ({ users: ids, holes = 18 }) {
    const users = await this.usersModel.get({ ids })
    if (users < 2 || users > 4) throw new UserInputError('This game requires 2-4 players.')
    const players = users.map(({ id }, index) => ({ id, alias: ALIASES[index] }))
    // Save game initial state
    return this.resourceModel.create({
      collection: this.collection,
      obj: {
        players,
        holes,
        scores: [],
        deck: newShuffledDeck(),
        events: []
      }
    })
  },

  async get ({ id }) {
    return this.resourceModel.get({
      collection: this.collection,
      query: {
        id
      }
    })
  },

  async update ({ id, update: { events, scores, deck } }) {
    return this.resourceModel.update({
      collection: this.collection,
      id,
      update: {
        events,
        deck,
        scores
      }
    })
  },

  async getNextTurn (game) {
    const nextTurn = NextTurn.get({ events: game.events, players: game.players })
    if (!nextTurn) {
      const updatedGame = await this.closeRound(game)
      return NextTurn.get({ events: updatedGame.events, players: updatedGame.players })
    }
  },

  getCurrentRound (game) {
    return new Board(game).playEvents().getSpots()
  },

  getBoard (game) {
    return new Board(game)
  },

  authorise ({ user, game }) {
    const [match] = game.players.filter(({ id }) => id === user.id)
    if (!match) throw new Error(`User ${user.id} is not allowed to access this board`)
    return match
  },
  
  authoriseEvent ({ game, event }) {
    const { player, action } = event
    const nextTurn = this.getNextTurn()
    if (nextTurn.player !== player) throw UserInputError(`It is not player ${player}'s turn to play.`)
    if (!nextTurn.actions.includes(action)) throw UserInputError(`Player ${player} must complete their turn by playing the actions '${nextTurn.actions.join("'  or '")}'`)
    return 'All good!'
  },

  async addEvent ({ id, event: ev, user }) {
    const up = await this.updateDeck (id)
    if (!id) throw new UserInputError('Cannot add event without a game id')
    const [game] = await this.get({ id })
    if (!game) throw UserInputError(`Game ${id} was not found`)
    const { alias } = this.authorise({ user, game })
    const event = { ...ev, player: alias }
    this.authoriseEvent({ game, event })
    const events = game.events.concat(event)
    // dry run
    this.authoriseEvent(game)
    return this.update({ id, update: { events } })
  },

  async closeRound (game) {
    const score = this.getBoard(game).getScores()
    const update = {
      scores: [...game.scores, score],
      deck: newShuffledDeck(),
      events: []
    }
    return this.update ({ 
      id: game.id, 
      update
    })
  }
}
