const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const app = express()
const cookieParser = require('cookie-parser')

module.exports = {
  name: 'server',
  deps: ['graphql'],
  app,

  async init () {
    console.log(new Date() instanceof Date)
    try {
      // Express App
      this.app.use(morgan('dev'))
      this.app.use(require('body-parser').json())
      this.app.use(cors(this.config.graphql.cors))
      this.app.use(cookieParser())
    } catch (err) {
      console.log('Server could not start due to Error: ', err)
    }
  },

  async startServer () {
    await new Promise((resolve, reject) => {
      // Start server
      const { port, host, protocol, exposedPort } = this.config.graphql
      this.graphql.addExpressMiddleware(this.app)
      this.app.listen(port, () => {
        console.log('Server listening on port', port, 'PID', process.pid)
        console.log(`Api available on : ${protocol}://${host}:${exposedPort}/graphql`)
        resolve()
      })
    })
  }
}
