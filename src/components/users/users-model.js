const bcrypt = require('bcryptjs')
const { UserInputError } = require('apollo-server')

module.exports = {
  name: 'usersModel',
  deps: ['mongo', 'resourceModel'],

  collection: 'users',

  async createCollection () {
    ['users'].map(this.mongo.db.createCollection)
  },

  async create ({ email, firstname, password }) {
    const [existingUser] = await this.get({ email })
    if (existingUser) throw new UserInputError(`User ${email} already exists`)
    const salt = bcrypt.genSaltSync(10)
    return this.resourceModel.create({
      collection: this.collection,
      obj: {
        email,
        firstname,
        hash: await bcrypt.hashSync(password, salt),
        createdAt: new Date()
      }
    })
  },

  async get ({ id, ids, email }) {
    return this.resourceModel.get({
      collection: this.collection,
      query: {
        id,
        ids,
        email
      }
    })
  },

  async update ({ id, update: { email, firstname } }) {
    if (email) {
      const [existingUser] = await this.get({ email })
      if (existingUser) throw new UserInputError(`User ${email} already exists`)
    }

    return this.resourceModel.update({
      collection: this.collection,
      id,
      update: {
        email,
        firstname
      }
    })
  }
}
