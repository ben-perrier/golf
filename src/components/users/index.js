module.exports = {
  name: 'users',
  deps: ['usersModel', 'graphql'],
  init () {
    this.graphql.registerSchema({
      typeDefs: this.graphql.load('users.graphql'),
      resolvers: {
        Mutation: {
          createUser: this.createUser,
          updateUser: this.updateUser
        }
      }
    })
  },

  async getUser (parent, args, context) {
    return (await this.usersModel.get(args))[0]
  },

  async createUser (parent, args, context) {
    return this.usersModel.create(args)
  },

  async updateUser (parent, args, context) {
    return this.usersModel.update(args)
  }
}
