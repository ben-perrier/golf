const { SchemaDirectiveVisitor } = require('apollo-server')
const { GraphQLList } = require('graphql')
const _ = require('lodash')

module.exports = {
  name: 'modelConnector',
  deps: ['graphql', 'usersModel', 'gamesModel'],
  init () {
    this.models = { users: this.usersModel, games: this.gamesModel }
    this.graphql.registerSchema({
      typeDefs: this.graphql.load('model.graphql'),
      schemaDirectives: {
        belongsTo: this.createBelongsToDirective()
      }
    })
  },

  createBelongsToDirective () {
    const component = this
    return class BelongsToDirective extends SchemaDirectiveVisitor {
      visitFieldDefinition (field, details) {
        field.resolve = async (parent, args, context) => {
          const options = { ...args }
          if (this.args.foreignKey) {
            if (!_.get(parent, this.args.foreignKey)) return null
            options[this.args.primaryKey] = _.get(parent, this.args.foreignKey)
          }
          if (!component[`${this.args.model}Model`]) throw new Error(`Model connector error: no model named ${this.args.model}`)
          const result = await component[`${this.args.model}Model`].get({ ...this.args.args, ...options })

          if (Array.isArray(result)) {
            return component.isList(field.type) ? result : result[0]
          }
          return result
        }
      }
    }
  },
  isList (type) {
    return type instanceof GraphQLList || (type.ofType && this.isList(type.ofType))
  },
  isConnectionType (type) {
    return (Array.isArray(type._interfaces) && type._interfaces.find(i => i.name === 'Connection')) || (type.ofType && this.isConnectionType(type.ofType))
  },
  isConnectionResult (res) {
    return res && Array.isArray(res.items)
  }
}
