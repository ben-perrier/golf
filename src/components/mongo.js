const { MongoClient } = require('mongodb')

module.exports = {
  name: 'mongo',
  async init () {
    await this.connect()
  },
  async connect () {
    const url = this.getConnectionUrl()
    try {
      this.client = await MongoClient.connect(url, { useNewUrlParser: true })
      this.db = this.client.db(this.config.database)
      this.isConnected = true
      // this.emit('connected', this.attempts)
      this.attempts = 1
      this.client.once('close', () => {
        // this.emit('disconnected', this)
        this.connect()
      })
    } catch (err) {
      console.log(`Mongo error connecting to ${url}`, err)
      this.isConnected = false
      err.attempts = this.attempts
      // this.emit('error', err)
      this.attempts++
      await new Promise((resolve) => setTimeout(resolve, 2000))
      return this.connect()
    }
  },

  getConnectionUrl () {
    const { username, password, hosts = [], database = '' } = this.config
    const stringifiedHosts = hosts.map(({ host, port }, index) => {
      const separator = (index < hosts.length - 1) ? ',' : ''
      return `${host}:${port}${separator}`
    })
    let auth = ''
    if (username && password) auth = `${username}:${password}@`
    else if (username) auth = `${username}@`
    return `mongodb://${auth}${stringifiedHosts}/${database}`
  }
}
