const { ApolloServer } = require('apollo-server-express')
const GraphQLJSON = require('graphql-type-json')
const { GraphQLScalarType } = require('graphql')
const { UserInputError } = require('apollo-server')
const _ = require('lodash')

module.exports = {
  name: 'graphql',
  deps: ['sessionsModel', 'authorisation'],

  init () {
    this.schemas = []
  },

  registerSchema (schema) {
    this.schemas.push(schema)
  },

  async addExpressMiddleware (app) {
    this.server = new ApolloServer({
      tracing: this.config.tracing,
      introspection: this.config.introspection,
      playground: this.config.playground,
      context: ({ req, res }) => ({ req, res, ...this.sessionsModel.get({ req }) }),
      engine: {
        apiKey: this.config.apolloEngine.key
      },
      typeDefs: [this.load('root.graphql'), this.load('authorisations.graphql'), ...this.schemas.map(schema => schema.typeDefs)],
      resolvers: [{
        JSON: GraphQLJSON,
        Date: new GraphQLScalarType({
          name: 'Date',
          description: 'Date',
          parseValue (value) {
            return new Date(value)
          },
          serialize (value) {
            return new Date(value).toISOString()
          },
          parseLiteral ({ value }) {
            if (!/\d{4}-\d{1,2}-\d{1,2}/.test(value)) throw UserInputError('Graphql Date input must be in format yyyy-mm-dd')
            return new Date(value)
          }
        })
      }, ...this.schemas.filter(schema => !!schema.resolvers).map(schema => {
        if (schema.resolvers) return schema.resolvers
      })],
      plugins: [this.authorisationPlugin()],
      schemaDirectives: Object.assign({}, ...this.schemas.map(schema => schema.schemaDirectives)),
      formatError: (error) => {
        const errorDetails = _.get(error, 'originalError.response.body')
        try {
          if (errorDetails) return JSON.parse(errorDetails)
        } catch (e) {}
        return error
      }
    })
    this.authorisation.registerAuthorisationGroups(this.server.schema)
    this.server.applyMiddleware({
      app,
      cors: false
    })
  },

  authorisationPlugin () {
    return {
      requestDidStart: () => {
        return {
          didResolveOperation: ({ request, operation, context }) => {
            const operationName = request.operationName || operation.selectionSet.selections[0].name.value
            this.authorisation.authorise({ context, operationName })
          }
        }
      }

    }
  },
  /**
   * @param {array | string} path: paths to the graphql model files to be loaded
   */
  load (path) {
    const paths = Array.isArray(path) ? path : [path]
    return paths.map(p => require('fs').readFileSync(`${__dirname}/../schemas/${p}`).toString('utf-8')).join(' ')
  }
}
