(async () => {
  const whales = require('whales')
  const container = await whales.createContainer({ eventsPerformanceLogging: 1, processPerformanceLogging: 1 })
  container.get('server').startServer()
})()
