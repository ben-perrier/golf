module.exports = {
  username: undefined,
  password: undefined,
  hosts: [{ host: '0.0.0.0', port: '4001' }],
  database: `db${process.env.NODE_ENV ? '-test' : ''}`
}
