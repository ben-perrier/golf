const { cors } = require('./infra.json')

const graphql = {
  protocol: 'http',
  host: 'localhost',
  port: 4000
}

module.exports = {
  graphql: {
    ...graphql,
    url: `${graphql.protocol}://${graphql.host}:${graphql.port}/graphql`,
    exposedPort: 4000,
    baseUrl: '',
    cors: {
      credentials: true,
      origin: cors.origin.map(str => new RegExp(str)) || false
    }
  }
}
