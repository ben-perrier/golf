
module.exports = {
  tracing: true,
  playground: true,
  introspection: true,
  baseUrl: '',
  apolloEngine: {
    key: 'not_in_dev'
  }
}
