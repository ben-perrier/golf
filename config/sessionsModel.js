const { cors } = require('./infra.json')

const {
  token: {
    encryptionKey
  }
} = require('./infra')

module.exports = {
  token: {
    encryptionKey
  },
  cookie: {
    name: 'token',
    lifespan: 1 * 60 * 60 * 1000,
    options: {
      credentials: true,
      origin: cors.origin.map(str => new RegExp(str)) || false
    }
  }
}
