const { requestForSessionTesting, gql } = require('./utilities/graphql')
const { recreateDBs } = require('./utilities/db')
const { activeUser, inactiveUser, usersPasswords } = require('./fixtures/users')
const { createComponent } = require('whales')
const { config: { cookie: { name: cookieName } } } = createComponent(require('../src/components/sessions/sessions-model'))

beforeAll(recreateDBs)

describe('Sessions model', () => {
  const loginQuery = gql`
  mutation createSession ($email: String!, $password: String!) {
    newSession: createSession (email: $email, password: $password) {
      user {
        id,
        firstname,
        email,
        createdAt,
        deleted
      }
    }
  }`

  describe('mutation createSession', () => {
    it('should allow active users to create a session', async () => {
      const variables = {
        email: activeUser.email,
        password: usersPasswords[activeUser.email]
      }
      const { json: { data, errors }, cookies: responseCookies } = await requestForSessionTesting(loginQuery, variables)
      if (Array.isArray(errors) && errors.length > 0) throw errors
      const { user: { id, email, firstname } } = data.newSession
      expect(id).toBe(activeUser.id)
      expect(email).toBe(activeUser.email)
      expect(firstname).toBe(activeUser.firstname)
      expect(responseCookies.split('=')[0]).toBe(cookieName)
    })

    it('should disallow existing users to create a session with a wrong password', async () => {
      const variables = {
        email: activeUser.email,
        password: `inexactPassword${new Date().getTime()}`
      }
      const resp = await requestForSessionTesting(loginQuery, variables)
      const { json: { errors }, cookies: responseCookies } = resp
      const [loginError] = errors
      expect(typeof loginError.message).toBe('string')
      expect(responseCookies).toBe(null)
    })

    it('should disallow inactive users to create a session', async () => {
      const variables = {
        email: inactiveUser.email,
        password: usersPasswords[inactiveUser.email]
      }
      const { json: { errors }, cookies: responseCookies } = await requestForSessionTesting(loginQuery, variables)
      const [loginError] = errors
      expect(typeof loginError.message).toBe('string')
      expect(responseCookies).toBe(null)
    })

    it('should disallow inexisting users to create a session', async () => {
      const variables = {
        email: `${new Date().getTime()}@inexisting.com`,
        password: 'secret'
      }
      const { json: { errors }, cookies: responseCookies } = await requestForSessionTesting(loginQuery, variables)
      const [loginError] = errors
      expect(typeof loginError.message).toBe('string')
      expect(responseCookies).toBe(null)
    })
  })

  describe('mutation logout', () => {
    it('should clear the users session cookie', async () => {
      const { cookies: logoutResponseCookies } = await requestForSessionTesting(gql`mutation { deleteSession }`, {}, activeUser)
      const [name, value] = logoutResponseCookies.split('=')
      expect(name).toBe(cookieName)
      expect(value.split(';')[0]).toBe('')
    })
  })
})
