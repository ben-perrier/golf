const users = [{
  id: '5e85bb0504a0c057152833ab',
  email: 'rupaul@potus.com',
  firstname: 'Rupaul',
  hash: '$2b$10$B3ot30dz7W7SJhk0VNIxA.nKnjn6GCaTNUB7yNUZJ/diQtSVNW.Qu',
  createdAt: new Date(0),
  deleted: false
}, {
  id: '5e85bb0504a0c057152833cd',
  email: 'detox@potus.com',
  firstname: 'Barack',
  createdAt: new Date(0),
  deleted: false
}, {
  id: '5e85bb0504a0c05715283323',
  email: 'alyssa.edwards@potus.com',
  firstname: 'Barack',
  createdAt: new Date(0),
  deleted: false
}, {
  id: '5e85bb0504a0c05715283334',
  email: 'barack.obama@potus.com',
  firstname: 'Barack',
  hash: '$2b$10$f7Xk84jCy4gB.gErRg/2B.NEi9zhU.QtGiZ3TxLsWHslxexXqQDf6',
  createdAt: new Date(0),
  deleted: true
}]

module.exports = {
  users,
  usersPasswords: {
    'rupaul@potus.com': 'secret',
    'barack.obama@potus.com': 'secret'
  },
  activeUser: users[0],
  inactiveUser: users[3]
}
