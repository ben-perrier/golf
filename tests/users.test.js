const { request, gql } = require('./utilities/graphql')
const { recreateDBs } = require('./utilities/db')
const { users } = require('./fixtures/users')

beforeAll(recreateDBs)

describe('Users model', () => {
  it('query user should allow to query single users', async () => {
    const variables = {
      id: users[0].id
    }
    const { data, errors } = await request(gql`
      query user($id: ID) {
        user(id: $id){
          id
          email
          firstname
          createdAt
          deleted
        }
      }`, variables)
    if (Array.isArray(errors) && errors.length > 0) throw errors
    const { id, email, firstname, createdAt, deleted } = data.user
    expect(typeof id).toBe('string')
    expect(typeof email).toBe('string')
    expect(typeof firstname).toBe('string')
    expect(typeof createdAt).toBe('string')
    expect(typeof deleted).toBe('boolean')
  })

  it('query users should allow to query multiple users', async () => {
    const variables = {
      ids: [users[1].id, users[2].id]
    }
    const { data, errors } = await request(gql`
      query users($ids: [ID]) {
        users(ids: $ids){
          id
          email
          firstname
          createdAt
          deleted
        }
      }`, variables)
    if (Array.isArray(errors) && errors.length > 0) throw errors
    expect(data.users.length).toBe(2)
    expect(variables.ids.includes(data.users[0].id)).toBe(true)
    expect(variables.ids.includes(data.users[1].id)).toBe(true)
  })

  it('mutation createUser should allow to create a user', async () => {
    const variables = {
      email: `benoit.perrier${new Date().getTime()}@me.me`,
      firstname: 'Ben',
      password: 'secretPassword'
    }
    const { data, errors } = await request(gql`
      mutation createUser ($email: String!, $firstname: String!, $password: String!) {
        createdUser: createUser (email: $email, firstname: $firstname, password: $password) {
          id,
          firstname,
          email,
          createdAt,
          deleted
        }
      }`, variables)
    if (Array.isArray(errors) && errors.length > 0) throw errors
    const { email, firstname } = data.createdUser
    expect(email).toBe(variables.email)
    expect(firstname).toBe(variables.firstname)
  })

  it('mutation updateUser should allow to update a user', async () => {
    const variables = {
      id: users[1].id,
      update: {
        firstname: 'Alaska',
        email: 'alaska@potus.com'
      }
    }
    const { data, errors } = await request(gql`
      mutation updateUser ($id: ID!, $update: UserUpdate!) {
        updatedUser: updateUser (id: $id, update: $update) {
          id,
          firstname,
          email,
          createdAt,
          deleted
        }
      }`, variables)
    if (Array.isArray(errors) && errors.length > 0) throw errors
    const { id, email, firstname, createdAt, deleted } = data.updatedUser
    expect(id).toBe(variables.id)
    expect(email).toBe(variables.update.email)
    expect(firstname).toBe(variables.update.firstname)
    expect(typeof createdAt).toBe('string')
    expect(typeof deleted).toBe('boolean')
  })
})
