module.exports = {
  initDBs,
  cleanDBs,
  recreateDBs
}
const { MongoClient, ObjectId } = require('mongodb')
const { users } = require('../fixtures/users')

const { createComponent } = require('whales')

let mongoClient = null

async function recreateDBs () {
  await cleanDBs()
  await initDBs()
}

async function initMongo () {
  if (!mongoClient) {
    const mongoComponent = createComponent(require('../../src/components/mongo'))
    const url = mongoComponent.getConnectionUrl()
    mongoClient = (await MongoClient.connect(url, { useNewUrlParser: true })).db(mongoComponent.config.database)
  }
}

async function initDBs () {
  await initMongo()

  return Promise.all([
    insertCollection('users', users)
  ])
}

async function cleanDBs () {
  await initMongo()
  return Promise.all([
    removeCollection('users', users)
  ])
}

async function insertCollection (collectionName, array) {
  return mongoClient.collection(collectionName).insertMany(array.map(itm => {
    const { id, ..._itm } = itm
    return {
      ..._itm,
      _id: new ObjectId(itm.id)
    }
  }))
}

async function removeCollection (collectionName, objects) {
  // remove objects by id
  if (objects) return mongoClient.collection(collectionName).deleteMany({ _id: { $in: objects.map(({ id }) => new ObjectId(id)) } })
  // remove entire collection
  return mongoClient.collection(collectionName).deleteMany({})
}
