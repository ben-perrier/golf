const fetch = require('node-fetch')
const { createComponent } = require('whales')
const sessionsModel = createComponent(require('../../src/components/sessions/sessions-model'))
const cookieName = sessionsModel.config.cookie.name

const { activeUser } = require('../fixtures/users')

const getToken = (user) => sessionsModel.createToken(user)

/**
 * Request client for session testing
 * @param {*} query
 * @param {*} variables
 * @param {*} user
 */
const requestForSessionTesting = async (query, variables = {}, user) => {
  const options = {
    credentials: 'include',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      variables,
      query
    }),
    method: 'POST'
  }
  if (user) options.headers.Cookie = `${cookieName}=${getToken(user)};`
  const resp = await fetch('http://localhost:4000/graphql', options)
  const json = await resp.json()
  const cookies = resp.headers.get('set-cookie')
  return { json, cookies }
}

/**
 * request client for data models
 * @param {*} query
 * @param {*} variables
 * @param {*} user: defaults to activeUser
 */
const request = async (query, variables, user = activeUser) => {
  const { json } = await requestForSessionTesting(query, variables, user)
  return json
}

const gql = (strs, ...values) => strs.map((str, i) => `${str}${values[i] || ''}`).join('')

module.exports = {
  gql,
  request,
  requestForSessionTesting
}
