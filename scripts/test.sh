#!/bin/bash
ENV=${1:-test}
kill -INT $(lsof -nPti tcp:4000)
echo "Run tests with env: ${ENV}"
yarn start:${ENV} &
PID=$!
#echo "PID "${PID}
while ! nc -z localhost 4000
do
  sleep 0.5
done
yarn test:${ENV}
TEST_RESULT=$?
newPid=$(lsof -nPti tcp:4000)
echo "Tests finished with exit code ${TEST_RESULT}, killing PID ${PID} newPid ${newPid}"
kill ${PID}
kill ${newPid}
while nc -z localhost 4000
do
  echo "Gateway still fecking running!"
  sleep 0.5
done
wait ${PID}
echo "Wait finished"
exit $TEST_RESULT