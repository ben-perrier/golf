# Graphql gateway - part of react-graphql-mongo-webapp

## Get started
Follow instructions in part 2 to create a new project

### Install
```shell
yarn
```

### Docker dependency
Starts up a mongo container on port 4001 with 2 dbs: `db` and `db-test` to run tests against
```shell
docker-compose up -d
```

### Start up
Gateway will start up against port 4000
```shell
yarn watch
```

### Linting
To fix linting errors
```
yarn fix-lint
```

### Testing
Starts up an instance of the gateway and runs tests against the mongo db `db-test`
```shell
yarn test
```

### Debug
To debug, run `yarn debug` and pick the process using the `debug pick` config profile.

### Default file indentation
Install editor config plugin in Visual studio.
See https://editorconfig.org/#overview


## Creating a new gateway
Todo list to create new project:
- Rename project in package.json, delete package.lock and run npm i
- Open .gitignore, uncomment infra.json and change passwords and encryption keys
- Change db name in docker-compose.yml and in config/mongo

## Mongo commands
Start up the mongo container
```shell
docker-compose up -d
```

Log in to the container
```
docker exec -it my-mongo bash
```

Log in to the mongo db
```
mongo -u YourUsername -p YourPasswordHere --authenticationDatabase db
```

see https://medium.com/faun/managing-mongodb-on-docker-with-docker-compose-26bf8a0bbae3
TODO: give it its own volume
